%define __os_install_post %{nil}

%define vmlinux_ver 5.10.0-126.0.0.66.oe2203.%{_arch}

Summary:       Intelligent ops toolkit for openEuler
Name:          gala-gopher
Version:       1.0.1
Release:       4
License:       Mulan PSL v2
URL:           https://gitee.com/openeuler/gala-gopher
Source:        %{name}-%{version}.tar.gz
BuildRoot:     %{_builddir}/%{name}-%{version}
BuildRequires: systemd cmake gcc-c++ elfutils-devel libcurl-devel
BuildRequires: clang >= 10.0.1 llvm java-1.8.0-openjdk-devel
BuildRequires: libconfig-devel librdkafka-devel libmicrohttpd-devel
BuildRequires: libbpf-devel >= 2:0.3 uthash-devel log4cplus-devel
Requires:      bash glibc elfutils zlib elfutils-devel bpftool
Requires:      dmidecode python3-psycopg2 python3-yaml erlang-eflame
Requires:      flamegraph-stackcollapse iproute libcurl
Requires:      libbpf >= 2:0.3 hostname kmod net-tools ethtool

Patch1:        add-configuration-instructions.patch
Patch2:        fix-stackprobe-memory-allocation-and-deallocation-er.patch
Patch3:        bugfix-add-check-if-thread-is-0.patch
Patch4:        some-bugfix.patch
Patch5:        add-support-to-pyroscope.patch
Patch6:        repair-stackprobe-caused-cpu-rush.patch
Patch7:        add-system_uuid-field-to-distinguish-client-when-pos.patch
Patch8:        fix-bug.patch
Patch9:        fix-ksliprobe-get-invalid-args-occasionally-at-start.patch

%description
gala-gopher is a low-overhead eBPF-based probes framework

%prep
%autosetup -n %{name}-%{version} -p1

%build
pushd build
sh build.sh --release %{vmlinux_ver}
popd

%install
install -d %{buildroot}/opt/gala-gopher
install -d %{buildroot}%{_bindir}
mkdir -p  %{buildroot}/usr/lib/systemd/system
install -m 0600 service/gala-gopher.service %{buildroot}/usr/lib/systemd/system/gala-gopher.service
pushd build
sh install.sh %{buildroot}%{_bindir} %{buildroot}/opt/gala-gopher
popd

%post
%systemd_post gala-gopher.service

%preun
%systemd_preun gala-gopher.service

%postun
%systemd_postun_with_restart gala-gopher.service

%files
%defattr(-,root,root)
%dir /opt/gala-gopher
%dir /opt/gala-gopher/extend_probes
%dir /opt/gala-gopher/meta
%dir /opt/gala-gopher/lib
%{_bindir}/*
%config(noreplace) /opt/gala-gopher/*.conf
/opt/gala-gopher/extend_probes/*
%config(noreplace) /opt/gala-gopher/extend_probes/*.conf
%exclude /opt/gala-gopher/extend_probes/*.pyc
%exclude /opt/gala-gopher/extend_probes/*.pyo
/opt/gala-gopher/meta/*
/opt/gala-gopher/lib/*
/usr/lib/systemd/system/gala-gopher.service

%changelog
* Tue Jan 31 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-4
- add net-tools and ethtool to Requires

* Sun Jan 29 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-3
- specify libbpf version in BuildRequires and add some Requires

* Sat Jan 14 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-2
- fix ksliprobe get invalid args occasionally at startup
- fix error print when starting gala-gopher
- add system_uuid field to distinguish client when post to pyroscope server
- repair stackprobe caused cpu rush
- add support to pyroscope
- bugfix: add check if thread is 0
- fix stackprobe memory allocation and deallocation errors.
- normalize time format in flamegraph svg filename

* Mon Dec 12 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-1
- Update to 1.0.1

* Thu Nov 17 2022 wo_cow <niuqianqian@huawei.com> - 1.0.0-7
- resolve patch conflict

* Thu Nov 17 2022 wo_cow <niuqianqian@huawei.com> - 1.0.0-6
- adapt libbpf v0.8

* Thu Nov 17 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-5
- add flamegraph-stackcollapse to Requires

* Tue Nov 15 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-4
- add systemd to BuildRequires to fix install/uninstall errors
  simplify patch application in %prep

* Mon Nov 14 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-3
- fix pgsliprobe

* Mon Nov 14 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-2
- add vmlinux for 22.03-LTS & 22.03-LTS-SP1

* Mon Nov 14 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-1
- Package init

